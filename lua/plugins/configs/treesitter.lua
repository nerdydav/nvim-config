return {
  ensure_installed = {
    "go",
    "lua",
    "rust",
    "python",
    "gleam",
  },
  highlight = {
    enable = true,
    use_languagetree = true,
  },
  indent = { enable = true },
}
