# NVIM

## Installation

### Backup

```
mv ~/.config/nvim ~/.config/nvim-backup
rm -rf ~/.local/share/nvim
```

### Install

To install this configuration

```
git clone https://pathtogit/nvim-config.git ~/.config/nvim
```

Then, open up neovim in order to download and install the base configuration packages.

## Modifying

### Custom Plugins

All custom plugins should be added to the `lua/custom/plugins.lua` file.

## Contribution

Please feel free to contribute to this configuration.
